/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	read_args(char **av, t_ls **ft_ls, int *ac)
{
	char	**p;

	p = av + 1;
	(*ac)--;
	while (*p)
	{
		if (**p == '-' && !(*ft_ls)->ls_files && !(*ft_ls)->ls_objects)
		{
			check_flags(*p, ft_ls);
			(*ac)--;
		}
		else
			check_ls_object(*p, ft_ls, NULL, 0);
		p++;
	}
	if (*ac == 0)
		check_ls_object(".", ft_ls, NULL, 0);
}

int		main(int ac, char **av)
{
	t_ls	*ft_ls;

	init_ls_struct(&ft_ls, av[0]);
	read_args(av, &ft_ls, &ac);
	if (ac > 1)
		ft_ls->flags |= MASK_ARGS;
	std_out(&ft_ls);
	return (0);
}
