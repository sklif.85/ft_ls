/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include "ft_ls.h"

void	fill_flags(char flag, t_ls **ft_ls)
{
	if (flag == 'l')
		(*ft_ls)->flags |= MASK_L;
	else if (flag == 'R')
		(*ft_ls)->flags |= MASK_R;
	else if (flag == 'a')
		(*ft_ls)->flags |= MASK_A;
	else if (flag == 'r')
		(*ft_ls)->flags |= MASK_RL;
	else if (flag == 't')
		(*ft_ls)->flags |= MASK_T;
	else if (flag == 'u')
		(*ft_ls)->flags |= MASK_U;
	else if (flag == 'f')
		(*ft_ls)->flags |= MASK_F;
	else if (flag == 'g')
		(*ft_ls)->flags |= MASK_G;
	else if (flag == 'd')
		(*ft_ls)->flags |= MASK_D;
	else if (flag == '1')
		(*ft_ls)->flags |= MASK_1;
}

void	check_flags(char *str, t_ls **ft_ls)
{
	char *p;

	p = ++str;
	while (*p)
	{
		ft_strchr(LSFLAGS, *p) ? fill_flags(*p, ft_ls) :
hendle_errors(1, *p, (*ft_ls)->path);
		p++;
	}
}

void	check_ls_object(char *path, t_ls **ft_ls,
t_ls_objects *tmp, int check_etc)
{
	struct stat	object_stat;

	if ((ft_strstr(path, "etc")) && path[(int)ft_strlen(path) - 1] == '\0')
		check_etc = 1;
	if (lstat(path, &object_stat) == 0 &&
	object_stat.st_nlink && path[0] != '-')
	{
		init_ls_object_struct(path, &tmp, object_stat);
		if ((*ft_ls)->flags & MASK_D)
			sorter(&tmp, &(*ft_ls)->ls_files, (*ft_ls)->flags);
		else
		{
			if (check_etc && ((*ft_ls)->flags & MASK_L))
				sorter(&tmp, &(*ft_ls)->ls_files, (*ft_ls)->flags);
			else if (S_ISDIR(tmp->object_stat.st_mode))
				sorter(&tmp, &(*ft_ls)->ls_objects, (*ft_ls)->flags);
			else if (!check_etc && !((*ft_ls)->flags & MASK_L))
				sorter(&tmp, &(*ft_ls)->ls_objects, (*ft_ls)->flags);
			else
				sorter(&tmp, &(*ft_ls)->ls_files, (*ft_ls)->flags);
		}
	}
	else
		perror_hendle((*ft_ls)->path, path);
}
