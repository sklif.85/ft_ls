/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorter_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 20:27:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 20:27:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		check_sort(t_ls_objects *p1_newfile, t_ls_objects *p2_file, int flags)
{
	char	*newfile;
	char	*file;

	if (!handle_sort_flag(p1_newfile, p2_file, flags))
	{
		newfile = p2_file->path;
		file = p1_newfile->path;
	}
	else
	{
		newfile = p1_newfile->path;
		file = p2_file->path;
	}
	while (*newfile || *file)
	{
		if ((int)*newfile < (int)*file)
			return (1);
		else if (((int)*newfile > (int)*file))
			return (0);
		newfile++;
		file++;
	}
	return (!*newfile ? 1 : 0);
}

void	push_backward(t_ls_objects **tmp, t_ls_objects **p)
{
	if (!(*p)->next)
	{
		(*p)->next = *tmp;
		(*tmp)->prev = *p;
	}
	else
	{
		(*tmp)->next = (*p)->next;
		(*tmp)->prev = *p;
		(*p)->next->prev = (*tmp);
		(*p)->next = (*tmp);
	}
}

void	insert_forward(t_ls_objects **tmp, t_ls_objects **p,
				t_ls_objects **data)
{
	(*tmp)->next = *p;
	(*tmp)->prev = (*p)->prev;
	!(*p)->prev ? *data = *tmp : 0;
	(*p)->prev ? (*p)->prev->next = *tmp : 0;
	(*p)->prev = *tmp;
}

void	sorter(t_ls_objects **tmp, t_ls_objects **objects, int flags)
{
	t_ls_objects	*p;

	p = *objects;
	if (!p)
		(*objects) = (*tmp);
	else
	{
		while (p->next)
		{
			if (check_sort((*tmp), p, flags))
			{
				insert_forward(tmp, &p, objects);
				return ;
			}
			p = p->next;
		}
		if (check_sort((*tmp), p, flags))
			insert_forward(tmp, &p, objects);
		else
			push_backward(tmp, &p);
	}
}

int		handle_sort_flag(t_ls_objects *object1,
							t_ls_objects *object2, int flags)
{
	long	curr_time;
	long	prev_time;

	curr_time = object1->object_stat.st_mtimespec.tv_sec;
	prev_time = object2->object_stat.st_mtimespec.tv_sec;
	if (flags & MASK_T)
	{
		if (curr_time > prev_time)
			return (0);
		return (1);
	}
	if (flags & MASK_RL)
		return (0);
	return (1);
}
