/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_in_multiple_columns.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

size_t	get_max_objects_len(t_ls_objects **objects)
{
	size_t	max_objects_len;
	char	***data;
	size_t	curr_obj_len;

	max_objects_len = 0;
	data = (*objects)->object_data;
	while (*data)
	{
		curr_obj_len = ft_strlen(**data);
		if (max_objects_len < curr_obj_len)
			max_objects_len = curr_obj_len;
		data++;
	}
	return (max_objects_len);
}

size_t	get_count_objects(t_ls_objects **objects)
{
	char	***data;

	data = (*objects)->object_data;
	while (*data)
		data++;
	return (size_t)(data - (*objects)->object_data);
}

size_t	get_count_row(size_t count_obj, size_t max_objects_len,
					size_t tty_width)
{
	float	count_row;

	count_row = (float)count_obj / (tty_width / (max_objects_len + 1));
	return (size_t)ceilf(count_row);
}

size_t	get_count_column(size_t max_objects_len, size_t tty_width)
{
	float	count_column;

	count_column = tty_width / (max_objects_len + 1);
	return (size_t)floorf(count_column);
}

size_t	get_tty_width(void)
{
	struct winsize	size;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
//	return (size.ws_col);
	return (80);
}
