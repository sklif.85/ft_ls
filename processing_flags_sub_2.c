/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing_flags_sub_2.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	get_xattr(char **permissions, char *path)
{
	acl_t		acl;
	acl_entry_t	obj;
	ssize_t		xattr;

	acl = acl_get_link_np(path, ACL_TYPE_EXTENDED);
	if (acl && acl_get_entry(acl, ACL_FIRST_ENTRY, &obj) == -1)
	{
		acl_free(acl);
		acl = NULL;
	}
	xattr = listxattr(path, NULL, 0, XATTR_NOFOLLOW);
	if (xattr < 0)
		xattr = 0;
	if (xattr > 0)
		(*permissions)[10] = '@';
	else if (acl != NULL)
		(*permissions)[10] = '+';
	else
		(*permissions)[10] = ' ';
	acl_free(acl);
}

void	get_object_links(struct stat object_stat, char ***p)
{
	*(*p) = ft_itoa(object_stat.st_nlink);
	(*p)++;
}

void	get_object_size(struct stat object_stat, char ***p)
{
	*(*p) = ft_itoa((int)object_stat.st_size);
	(*p)++;
}

void	get_object_owner(struct stat object_stat, char ***p)
{
	struct passwd	*pwuid;

	pwuid = getpwuid(object_stat.st_uid);
	*(*p) = ft_strdup(pwuid->pw_name);
	(*p)++;
}

void	get_object_group(struct stat object_stat, char ***p)
{
	struct group	*grp;

	grp = getgrgid(object_stat.st_gid);
	*(*p) = ft_strdup(grp->gr_name);
	(*p)++;
}
