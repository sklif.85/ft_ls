/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing_flags_sub_1.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	get_object_name(t_ls_objects **objects, char *object_name,
				char ***p, int i)
{
	(*objects)->object_data[i][6] = ft_strdup(object_name);
	(*p)++;
}

void	get_path(char **path, t_ls_objects **objects, char *object_name)
{
	if (!ft_strcmp((*objects)->dirent->d_name, CURR_FOLDER))
		ft_strjoin3(path, (*objects)->path, "/", CURR_FOLDER);
	else if (!ft_strcmp((*objects)->dirent->d_name, PREV_FOLDER))
	{
		if ((*objects)->path[ft_strlen((*objects)->path) - 1] == '/')
			ft_strjoin2(path, (*objects)->path, object_name);
		else
			ft_strjoin3(path, (*objects)->path, "/", object_name);
	}
	else
	{
		if ((*objects)->path[ft_strlen((*objects)->path) - 1] == '/')
			ft_strjoin2(path, (*objects)->path, object_name);
		else
			ft_strjoin3(path, (*objects)->path, "/", object_name);
	}
}

void	get_object_flag(struct stat object_stat, char **permissions)
{
	if (S_ISLNK(object_stat.st_mode))
		(*permissions)[0] = 'l';
	else if (S_ISREG(object_stat.st_mode))
		(*permissions)[0] = '-';
	else if (S_ISDIR(object_stat.st_mode))
		(*permissions)[0] = 'd';
	else if (S_ISCHR(object_stat.st_mode))
		(*permissions)[0] = 'c';
	else if (S_ISBLK(object_stat.st_mode))
		(*permissions)[0] = 'b';
	else if (S_ISFIFO(object_stat.st_mode))
		(*permissions)[0] = 'p';
	else if (S_ISSOCK(object_stat.st_mode))
		(*permissions)[0] = 's';
	else
		(*permissions)[0] = '\0';
}

ssize_t	get_link_object(char **name_link, char *path)
{
	struct stat	slink_stat;
	ssize_t		link_len;

	if (lstat(path, &slink_stat) == -1)
	{
		perror("lstat");
		exit(EXIT_FAILURE);
	}
	*name_link = malloc((size_t)(slink_stat.st_size + 1));
	link_len = readlink(path, *name_link, (size_t)(slink_stat.st_size + 1));
	(*name_link)[slink_stat.st_size] = '\0';
	return (link_len);
}

void	get_object_permissions(struct stat object_stat, char **permissions,
						char ***p, char *path)
{
	int		i;
	char	*base;

	i = -1;
	base = "xwrxwrxwr";
	while (++i <= 8)
	{
		if (object_stat.st_mode & (1 << i))
			(*permissions)[9 - i] = base[i];
		else
			(*permissions)[9 - i] = '-';
	}
	if (object_stat.st_mode & S_ISUID)
		(*permissions)[3] = (char)((object_stat.st_mode & S_IXUSR) ? 's' : 'S');
	else if (object_stat.st_mode & S_ISGID)
		(*permissions)[6] = (char)((object_stat.st_mode & S_IXGRP) ? 's' : 'l');
	else if (object_stat.st_mode & S_ISVTX)
		(*permissions)[9] = (char)((object_stat.st_mode & S_IXOTH) ? 't' : 'T');
	get_xattr(permissions, path);
	*(*p) = ft_strdup(*permissions);
	(*p)++;
}
