/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing_flags.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	processing_flag_l(char *object_name, t_ls_objects **objects,
					int i, int flags)
{
	char		**p;
	struct stat	object_stat;
	char		*permissions;
	char		*path;

	path = NULL;
	p = (*objects)->object_data[i];
	permissions = (char *)malloc(sizeof(char) * 12);
	ft_bzero(permissions, sizeof(char) * 12);
	get_path(&path, objects, object_name);
	lstat(path, &object_stat);
	get_object_flag(object_stat, &permissions);
	get_object_permissions(object_stat, &permissions, &p, path);
	get_object_links(object_stat, &p);
	get_object_owner(object_stat, &p);
	get_object_group(object_stat, &p);
	get_object_size(object_stat, &p);
	get_object_time(object_stat, &p, flags);
	get_object_name(objects, object_name, &p, i);
	get_blocks_size(objects, object_stat, i);
	free(permissions);
	free(path);
}

void	processing_flag_l_files(t_ls_objects **objects, int flags)
{
	char	**p;
	char	*permissions;

	p = *(*objects)->object_data;
	permissions = (char *)malloc(sizeof(char) * 12);
	ft_bzero(permissions, sizeof(char) * 12);
	get_object_flag((*objects)->object_stat, &permissions);
	get_object_permissions((*objects)->object_stat, &permissions, &p,
					(*objects)->path);
	get_object_links((*objects)->object_stat, &p);
	get_object_owner((*objects)->object_stat, &p);
	get_object_group((*objects)->object_stat, &p);
	get_object_size((*objects)->object_stat, &p);
	get_object_time((*objects)->object_stat, &p, flags);
	*p = ft_strdup((*objects)->path);
	free(permissions);
}

void	get_tty_data(t_ls_objects **objects, size_t **tty_data)
{
	size_t	tty_width;
	size_t	max_objects_len;
	size_t	count_obj;
	size_t	count_column;
	size_t	count_row;

	tty_width = get_tty_width();
	max_objects_len = get_max_objects_len(objects);
	count_obj = get_count_objects(objects);
	count_column = get_count_column(max_objects_len, tty_width);
	count_row = get_count_row(count_obj, max_objects_len, tty_width);
	(*tty_data)[0] = tty_width;
	(*tty_data)[1] = max_objects_len;
	(*tty_data)[2] = count_obj;
	(*tty_data)[3] = count_column;
	(*tty_data)[4] = count_row;
}

void	get_blocks_size(t_ls_objects **objects, struct stat object_stat, int i)
{
	(*objects)->object_data[i][7] = ft_itoa((int)object_stat.st_blocks);
}
