/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	init_ls_struct(t_ls **ft_ls, char *path)
{
	*ft_ls = (t_ls *)malloc(sizeof(t_ls));
	(*ft_ls)->flags = 0;
	(*ft_ls)->path = ft_strdup(path);
	(*ft_ls)->ls_objects = NULL;
	(*ft_ls)->ls_files = NULL;
}

void	init_ls_object_struct(char *path, t_ls_objects **object,
								struct stat object_stat)
{
	(*object) = (t_ls_objects *)malloc(sizeof(t_ls_objects));
	(*object)->name = extruct_name(path);
	(*object)->path = ft_strdup(path);
	(*object)->object_stat = object_stat;
	(*object)->object_data = NULL;
	(*object)->ls_objects = NULL;
	(*object)->dirent = NULL;
	(*object)->dir = NULL;
	(*object)->next = NULL;
	(*object)->prev = NULL;
}

void	init_object_data_element(char ***object_data, int flags)
{
	int n;

	n = flags & MASK_L ? 8 : 2;
	*object_data = (char **)malloc(sizeof(char *) * n);
	(*object_data)[n - 1] = NULL;
}

void	init_file_data(t_ls_objects **objects, int flags)
{
	int n;

	n = flags & MASK_L ? 8 : 2;
	(*objects)->object_data = (char ***)malloc(sizeof(char **) * 2);
	(*objects)->object_data[0] = (char **)malloc(sizeof(char *) * n);
	(*objects)->object_data[1] = NULL;
}

void	init_next_object(t_ls_objects **objects, int *flags,
				char *path, struct stat object_stat)
{
	t_ls_objects	*tmp;

	if (*flags & MASK_RL)
	{
		tmp = NULL;
		init_ls_object_struct(path, &tmp, object_stat);
		tmp->next = (*objects)->ls_objects;
		(*objects)->ls_objects = tmp;
	}
	else
	{
		tmp = (*objects)->ls_objects;
		while (tmp->next)
			tmp = tmp->next;
		init_ls_object_struct(path, &tmp->next, object_stat);
	}
}
