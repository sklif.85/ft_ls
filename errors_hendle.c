/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors_hendle.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:07:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:07:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	hendle_errors(int id, char c, char *path)
{
	id == 1 ? ft_printf("%s: illegal option -- %c\n", path, c) : 0;
	ft_printf("usage: %s [-Radfghlru1] [file ...]\n", path);
	exit(1);
}

void	perror_hendle(char *path, char *name)
{
	char	*tmp;

	ft_strjoin2(&tmp, path, name);
	perror(tmp);
	free(tmp);
}
