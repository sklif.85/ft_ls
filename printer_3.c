/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer_3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_in_multiple_columns_reverse(t_ls_objects **objects,
									size_t *tty_data, int row)
{
	int		column;
	int		index;
	size_t	j;

	index = 0;
	j = tty_data[2] - 1;
	while (row < (int)tty_data[4])
	{
		column = -1;
		while (++column < (int)tty_data[3])
		{
			if (index >= (int)tty_data[2])
				break ;
			ft_printf("%-*s", (int)tty_data[1] + 1,
				(*objects)->object_data[j - index][0]);
			index += tty_data[4];
		}
		if (index >= (int)tty_data[2] && row == (int)tty_data[4] - 1)
		{
			ft_printf("\n");
			break ;
		}
		row < (int)tty_data[4] ? ft_printf("\n") : 0;
		index = ++row;
	}
}

void	print_in_multiple_columns_normal(t_ls_objects **objects,
									size_t *tty_data)
{
	int	row;
	int	column;
	int	index;

	row = 0;
	index = 0;
	while (row < (int)tty_data[4])
	{
		column = -1;
		while (++column < (int)tty_data[3])
		{
			if (index >= (int)tty_data[2])
				break ;
			ft_printf("%-*s", (int)tty_data[1] + 1,
				(*objects)->object_data[index][0]);
			index += tty_data[4];
		}
		if (index >= (int)tty_data[2] && row == (int)tty_data[4] - 1)
		{
			ft_printf("\n");
			break ;
		}
		row < (int)tty_data[4] ? ft_printf("\n") : 0;
		index = ++row;
	}
}

void	print_in_multiple_columns(t_ls_objects **objects, int flags)
{
	size_t	*tty_data;

	if (!(*objects)->object_data)
		return ;
	tty_data = (size_t *)malloc(sizeof(size_t ) * 5);
	get_tty_data(objects, &tty_data);
	if (flags & MASK_RL)
		print_in_multiple_columns_reverse(objects, tty_data, 0);
	else
		print_in_multiple_columns_normal(objects, tty_data);
	free(tty_data);
}

void	make_it_print(t_ls_objects **objects, int flags)
{
	if (flags & MASK_L)
	{
		get_full_link_name(objects, 'd');
		print_with_params(objects, flags);
	}
	else if (flags & MASK_1)
		print_with_out_params(objects, flags);
	if (!(flags & MASK_1) && !(flags & MASK_L))
		print_in_multiple_columns(objects, flags);
}
