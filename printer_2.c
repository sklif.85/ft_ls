/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_files_with_params(t_ls_objects **objects, int flags)
{
	t_ls_objects	*p;
	size_t			n1;
	size_t			n2;
	char			***data;

	p = *objects;
	n1 = get_width_file(objects, 1, flags);
	n2 = get_width_file(objects, 4, flags);
	while (p)
	{
		data = p->object_data;
		if (flags & MASK_G)
			ft_printf("%s  %s  %s %*s %s %s\n", (*data)[0], (*data)[1],
	(*data)[3], ft_strlen((*data)[4]), (*data)[4], (*data)[5], (*data)[6]);
		else
			ft_printf("%s %*s %s  %s  %*s %s %s\n", (*data)[0], n1,
	(*data)[1], (*data)[2], (*data)[3], n2, (*data)[4], (*data)[5], (*data)[6]);
		p = p->next;
	}
}

void	print_files_with_out_params(t_ls_objects **files, int flags)
{
	t_ls_objects	*p;

	p = *files;
	if (flags & MASK_1)
	{
		while (p)
		{
			ft_printf("%s\n", p->path);
			p = p->next;
		}
	}
	else
		print_files_in_multiple_columns(files, get_tty_width());
}
