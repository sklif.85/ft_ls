/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft/libft.h"
# include "libft/ft_printf.h"
# include "libft/get_next_line.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <dirent.h>
# include <sys/errno.h>
# include <pwd.h>
# include <grp.h>
# include <sys/ioctl.h>
# include <err.h>
# include <math.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/xattr.h>
# include <sys/types.h>
# include <sys/acl.h>
# include <stdio.h>

# define LSFLAGS "lRartufgd1"
# define MASK_L 1
# define MASK_R 2
# define MASK_A 4
# define MASK_RL 8
# define MASK_T 16
# define MASK_U 32
# define MASK_F 64
# define MASK_G 128
# define MASK_D 256
# define MASK_1 512
# define MASK_REC 1024
# define MASK_ARGS 2048
# define LSTAT_CHECK 4096
# define CURR_FOLDER "."
# define PREV_FOLDER ".."
# define HALF_A_YEAR 15778463

typedef struct			s_ls_objects
{
	char				*path;
	char				*name;
	struct dirent		*dirent;
	DIR					*dir;
	char				***object_data;
	struct stat			object_stat;
	struct s_ls_objects	*ls_objects;
	struct s_ls_objects	*prev;
	struct s_ls_objects	*next;
}						t_ls_objects;

typedef struct			s_ls
{
	int					flags;
	char				*path;
	t_ls_objects		*ls_objects;
	t_ls_objects		*ls_files;
}						t_ls;

int						main(int ac, char **av);
void					init_ls_struct(t_ls **ft_ls, char *path);
void					init_object_data_element(char ***object_data,
int flags);
void					init_file_data(t_ls_objects **objects, int flags);
void					read_args(char **av, t_ls **ft_ls, int *ac);
void					check_flags(char *str, t_ls **ft_ls);
void					check_ls_object(char *path, t_ls **ft_ls,
t_ls_objects *tmp, int check_etc);
char					*extruct_name(char *path);
void					init_next_object(t_ls_objects **objects, int *flags,
char *path, struct stat object_stat);
void					sorter(t_ls_objects **ttmp, t_ls_objects **tobjects,
int flags);
int						check_sort(t_ls_objects *newfile, t_ls_objects *file,
int flags);
void					push_backward(t_ls_objects **tmp, t_ls_objects **p);
void					insert_forward(t_ls_objects **tmp, t_ls_objects **p,
t_ls_objects **data);
void					handle_objects(t_ls_objects *objects, int flag);
void					handle_files(t_ls_objects **files, int flags);
void					open_object(t_ls_objects **objects, int flag);
void					handle_folder(char *name, t_ls_objects **objects,
int *flags);
void					handle_name_space(t_ls_objects **objects, int flags);
void					handle_flags(char *object_name, t_ls_objects **objects,
int flags, int i);
void					init_ls_object_struct(char *path, t_ls_objects **tmp,
struct stat object_stat);
void					std_out(t_ls **ft_ls);
void					processing_flag_l(char *object_name,
t_ls_objects **objects, int i, int flags);
void					processing_flag_l_files(t_ls_objects **objects,
int flags);
void					get_path(char **path, t_ls_objects **objects,
char *object_name);
void					get_object_name(t_ls_objects **objects,
char *object_name, char ***p, int i);
void					get_blocks_size(t_ls_objects **objects,
struct stat object_stat, int i);
void					get_object_flag(struct stat robject_stat,
char **permissions);
ssize_t					get_link_object(char **name_link, char *path);
void					get_object_permissions(struct stat object_stat,
char **permissions, char ***p, char *path);
void					get_xattr(char **permissions, char *path);
void					get_object_links(struct stat object_stat, char ***p);
void					get_object_size(struct stat object_stat, char ***p);
void					get_object_owner(struct stat object_stat, char ***p);
void					get_object_time(struct stat object_stat, char ***p,
int flags);
char					*get_timestamp(time_t tspec, char *tmodif);
void					get_object_group(struct stat object_stat, char ***p);
void					make_it_print(t_ls_objects **objects, int flags);
void					print_with_params(t_ls_objects **objects, int flags);
void					print_with_out_params(t_ls_objects **objects,
int flags);
void					print_files_with_params(t_ls_objects **objects,
int flags);
void					print_files_with_out_params(t_ls_objects **files,
int flags);
void					print_in_multiple_columns(t_ls_objects **objects,
int flags);
void					print_files_in_multiple_columns(t_ls_objects **files,
size_t tty_width);
void					print_in_multiple_columns_reverse(t_ls_objects **o,
size_t *tty_data, int row);
void					handle_print_files_in_multiple_columns(t_ls_objects **f,
size_t max_objects_len, size_t count_column, size_t count_row);
size_t					get_max_file_len(t_ls_objects **files);
size_t					get_count_file(t_ls_objects **files);
size_t					*get_total_width(t_ls_objects **objects);
void					reverse_print(char ***data, t_ls_objects **objects,
int flags);
void					microservice_printf(char ***data, int flags,
size_t *width);
size_t					get_width(char ***data, int n);
size_t					get_width_file(t_ls_objects **objects, int n,
int flags);
int						count_blocks(char ***data);
char					***ft_realloc3(char ***object_data, int i);
char					***ft_realloc(char ***object_data, int i);
void					free3(char ***object_data);
void					ft_strjoin3(char **dest, char *s1, char *s2, char *s3);
void					ft_strjoin2(char **dest, char *s1, char *s2);
void					handle_sort(t_ls_objects **object, int flags, int pos);
int						handle_sort_flag(t_ls_objects *object1,
t_ls_objects *object2, int flags);
void					sorting_by_common(char ****object_data, int j,
int pos);
void					sorting_by_time(char ****object_data, int j,
char *path, int i);
int						sort_checker_by_common(char **prev, char **curr,
int j);
int						sort_checker_by_time(char **prev, char **curr, int j,
char *path);
void					make_sort(char ****object_data, int i);
size_t					get_tty_width();
void					get_tty_data(t_ls_objects **objects,
size_t **tty_data);
size_t					get_max_objects_len(t_ls_objects **objects);
size_t					get_count_objects(t_ls_objects **objects);
size_t					get_count_row(size_t count_obj,
size_t max_objects_len, size_t tty_width);
size_t					get_count_column(size_t max_objects_len,
size_t tty_width);
void					get_full_link_name(t_ls_objects **objects, char flag);
void					hendle_errors(int id, char c, char *path);
void					perror_hendle(char *path, char *name);

#endif
