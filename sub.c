/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sub.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:07:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:07:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	get_full_link_name(t_ls_objects **objects, char flag)
{
	char	***data;
	char	*name_link;
	char	*path;
	char	*tmp;

	data = (*objects)->object_data;
	if (!data)
		return ;
	while (*data)
	{
		if (*(*(*data)) && *(*data)[0] == 'l')
		{
			if (flag == 'f')
				path = (*objects)->path;
			else
				ft_strjoin3(&path, (*objects)->path, "/", (*data)[6]);
			get_link_object(&name_link, path);
			tmp = (*data)[6];
			ft_strjoin3(&(*data)[6], (*data)[6], " -> ", name_link);
			free(tmp);
			free(name_link);
			free(path);
		}
		data++;
	}
}

void	ft_strjoin3(char **dest, char *s1, char *s2, char *s3)
{
	int len1;
	int len2;
	int len3;
	int i;
	int j;

	len1 = (int)ft_strlen(s1);
	len2 = (int)ft_strlen(s2);
	len3 = (int)ft_strlen(s3);
	i = -1;
	j = -1;
	*dest = (char *)malloc(sizeof(char) * (len1 + len2 + len3 + 1));
	while (++i < len1)
		(*dest)[++j] = s1[i];
	i = -1;
	while (++i < len2)
		(*dest)[++j] = s2[i];
	i = -1;
	while (++i < len3)
		(*dest)[++j] = s3[i];
	(*dest)[++j] = '\0';
}

void	ft_strjoin2(char **dest, char *s1, char *s2)
{
	int len1;
	int len2;
	int i;
	int j;

	len1 = (int)ft_strlen(s1);
	len2 = (int)ft_strlen(s2);
	i = -1;
	j = -1;
	*dest = (char *)malloc(sizeof(char) * (len1 + len2 + 1));
	while (++i < len1)
		(*dest)[++j] = s1[i];
	i = -1;
	while (++i < len2)
		(*dest)[++j] = s2[i];
	(*dest)[++j] = '\0';
}

char	*extruct_name(char *path)
{
	if (!ft_strcmp(path, CURR_FOLDER))
		return (CURR_FOLDER);
	else if (!ft_strcmp(path, PREV_FOLDER))
		return (PREV_FOLDER);
	else if (ft_strrchr(path, '/'))
		return (ft_strndup(ft_strrchr(path, '/') + 1,
					path + ft_strlen(path) - ft_strrchr(path, '/')));
	else
		return (path);
}
