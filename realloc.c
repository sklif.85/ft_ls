/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	free3(char ***object_data)
{
	int	i;
	int	j;

	i = -1;
	while (object_data[++i])
	{
		j = -1;
		while (object_data[i][++j])
			free(object_data[i][j]);
		free(object_data[i]);
	}
	free(object_data);
}

char	***ft_realloc(char ***object_data, int i)
{
	char	***tmp;
	int		j;

	tmp = (char ***)malloc(sizeof(char **) * (i + 2));
	j = -1;
	while (object_data[++j])
	{
		tmp[j] = object_data[j];
		object_data[j] = NULL;
	}
	tmp[j] = NULL;
	tmp[j + 1] = NULL;
	return (tmp);
}

char	***ft_realloc3(char ***object_data, int i)
{
	char	***tmp;

	if (!object_data)
	{
		tmp = (char ***)malloc(sizeof(char **) * 2);
		tmp[0] = NULL;
		tmp[1] = NULL;
	}
	else
		tmp = ft_realloc(object_data, i);
	return (tmp);
}
