/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stdout.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	std_out(t_ls **ft_ls)
{
	if ((*ft_ls)->flags & MASK_G)
		(*ft_ls)->flags |= MASK_L;
	if ((*ft_ls)->flags & MASK_F)
		(*ft_ls)->flags |= MASK_A;
	if ((*ft_ls)->flags & MASK_D)
		(*ft_ls)->flags ^= MASK_R;
	if ((*ft_ls)->flags & MASK_L && (*ft_ls)->flags & MASK_1)
		(*ft_ls)->flags ^= MASK_1;
	if ((*ft_ls)->ls_files)
	{
		handle_files(&(*ft_ls)->ls_files, (*ft_ls)->flags);
		if ((*ft_ls)->ls_objects)
			ft_printf("\n");
	}
	if ((*ft_ls)->ls_objects)
		handle_objects((*ft_ls)->ls_objects, (*ft_ls)->flags);
}

void	handle_files(t_ls_objects **files, int flags)
{
	t_ls_objects	*p;

	p = *files;
	while (p)
	{
		if (flags & MASK_L)
		{
			init_file_data(&p, flags);
			processing_flag_l_files(&p, flags);
			get_full_link_name(&p, 'f');
		}
		p = p->next;
	}
	if (flags & MASK_L)
		print_files_with_params(files, flags);
	else
		print_files_with_out_params(files, flags);
}
