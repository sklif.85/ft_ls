/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 20:27:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 20:27:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		sort_checker_by_common(char **prev, char **curr, int j)
{
	int		i;
	char	*p;
	char	*c;

	i = 0;
	p = prev[j];
	c = curr[j];
	while (p[i] && c[i])
	{
		if (p[i] > c[i])
			return (0);
		if (p[i] < c[i])
			return (1);
		i++;
	}
	return (1);
}

int		sort_checker_by_time(char **prev, char **curr, int j, char *path)
{
	char		*p;
	char		*c;
	struct stat	curr_stat;
	struct stat	prev_stat;

	p = ft_strjoin(ft_strjoin(path, "/"), prev[j]);
	c = ft_strjoin(ft_strjoin(path, "/"), curr[j]);
	lstat(c, &curr_stat);
	lstat(p, &prev_stat);
	if (curr_stat.st_mtimespec.tv_sec > prev_stat.st_mtimespec.tv_sec)
		return (1);
	else if (curr_stat.st_mtimespec.tv_sec == prev_stat.st_mtimespec.tv_sec)
		return (2);
	return (3);
}

void	sorting_by_common(char ****object_data, int j, int i)
{
	while (i)
	{
		if (!sort_checker_by_common((*object_data)[i - 1],
								(*object_data)[i], j))
			make_sort(object_data, i);
		else
			break ;
		i--;
	}
}

void	sorting_by_time(char ****object_data, int j, char *path, int i)
{
	int check;

	while (i)
	{
		check = sort_checker_by_time((*object_data)[i - 1],
									(*object_data)[i], j, path);
		if (check == 1)
			make_sort(object_data, i);
		else if (check == 2)
		{
			if (!sort_checker_by_common((*object_data)[i - 1],
									(*object_data)[i], j))
				make_sort(object_data, i);
		}
		else
			break ;
		i--;
	}
}

void	handle_sort(t_ls_objects **object, int flags, int pos)
{
	int	j;

	j = flags & MASK_L ? 6 : 0;
	if (flags & MASK_T)
		sorting_by_time(&(*object)->object_data, j, (*object)->path, pos);
	else if (flags & MASK_L)
		sorting_by_common(&(*object)->object_data, j, pos);
	else if (flags & MASK_F)
		return ;
	else
		sorting_by_common(&(*object)->object_data, j, pos);
}
