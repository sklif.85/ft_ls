/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_objects.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	handle_objects(t_ls_objects *objects, int flags)
{
	t_ls_objects	*p;

	p = objects;
	while (p)
	{
		if (p->prev || (flags & MASK_REC))
			ft_printf("\n");
		open_object(&p, flags);
		p = p->next;
	}
}

void	open_object(t_ls_objects **objects, int flags)
{
	char *tmp;

	(*objects)->dir = opendir((*objects)->path);
	if ((*objects)->dir == NULL)
	{
		ft_strjoin2(&tmp, (*objects)->path, ":");
		ft_printf("%s\n", tmp);
		perror_hendle("ls: ", (*objects)->name);
		free(tmp);
	}
	else
	{
		if (flags & MASK_ARGS && !(flags & MASK_D))
			ft_printf("%s:\n", (*objects)->path);
		else
			flags ^= MASK_ARGS;
		handle_name_space(objects, flags);
	}
	if (flags & MASK_R)
	{
		flags |= MASK_REC;
		handle_objects((*objects)->ls_objects, flags);
	}
}

void	handle_name_space(t_ls_objects **objects, int flags)
{
	int		i;
	char	***tmp;

	i = 0;
	while (((*objects)->dirent = readdir((*objects)->dir)))
	{
		if (flags & MASK_A || (*objects)->dirent->d_name[0] != '.')
		{
			tmp = ft_realloc3((*objects)->object_data, i);
			if ((*objects)->object_data)
				free3((*objects)->object_data);
			(*objects)->object_data = tmp;
			init_object_data_element(&((*objects)->object_data[i]), flags);
			handle_folder((*objects)->dirent->d_name, objects, &flags);
			if (flags & LSTAT_CHECK)
				return ;
			handle_flags((*objects)->dirent->d_name, objects, flags, i);
			handle_sort(objects, flags, i);
			i++;
		}
	}
	closedir((*objects)->dir);
	make_it_print(objects, flags);
}

void	handle_folder(char *name, t_ls_objects **objects, int *flags)
{
	struct stat		object_stat;
	char			*path;

	if ((*objects)->path[ft_strlen((*objects)->path) - 1] == '/')
		ft_strjoin2(&path, (*objects)->path, name);
	else
		ft_strjoin3(&path, (*objects)->path, "/", name);
	if (lstat(path, &object_stat) < 0)
	{
		*flags |= LSTAT_CHECK;
		return ;
	}
	if ((S_ISDIR(object_stat.st_mode) && (*flags & MASK_R)) &&
		(ft_strcmp((*objects)->dirent->d_name, CURR_FOLDER) &&
				ft_strcmp((*objects)->dirent->d_name, PREV_FOLDER)))
	{
		if (!(*objects)->ls_objects)
			init_ls_object_struct(path, &(*objects)->ls_objects, object_stat);
		else
			init_next_object(objects, flags, path, object_stat);
	}
	free(path);
}

void	handle_flags(char *object_name, t_ls_objects **objects,
						int flags, int i)
{
	if (flags & MASK_L)
		processing_flag_l(object_name, objects, i, flags);
	if (!(flags & MASK_L))
		*((*objects)->object_data[i]) = ft_strdup(object_name);
//	if (flags & MASK_1 && !(flags & MASK_L))
//		ft_printf("\n");
}
