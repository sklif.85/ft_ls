/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing_flags_sub_3.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_timestamp(time_t tspec, char *tmodif)
{
	time_t	t;
	time_t	tmp_time;
	char	*t1;
	char	*t2;
	char	*tres;

	time(&t);
	tmp_time = t - tspec;
	if ((int)HALF_A_YEAR <= (int)tmp_time || (int)tmp_time <= 3)
	{
		t1 = ft_strndup(tmodif + 4, ft_strlen(tmodif) - 18);
		t2 = ft_strndup(tmodif + 19, 5);
		ft_strjoin2(&tres, t1, t2);
		free(t1);
		free(t2);
		return (tres);
	}
	else
		return (ft_strndup(tmodif + 4, ft_strlen(tmodif) - 13));
}

void	get_object_time(struct stat object_stat, char ***p, int flags)
{
	time_t	tspec;
	char	*tmodif;

	if (flags & MASK_U)
		tspec = object_stat.st_atimespec.tv_sec;
	else
		tspec = object_stat.st_mtimespec.tv_sec;
	tmodif = ctime(&tspec);
	*(*p) = get_timestamp(tspec, tmodif);
	(*p)++;
}

size_t	get_width_file(t_ls_objects **objects, int n, int flags)
{
	size_t			width;
	t_ls_objects	*p;
	size_t			len;

	width = 0;
	p = *objects;
	while (p)
	{
		if (flags & MASK_L)
			len = ft_strlen((*p->object_data)[n]);
		else
			len = ft_strlen(p->path);
		if (len > width)
			width = len;
		p = p->next;
	}
	return (width);
}

size_t	get_width(char ***data, int n)
{
	size_t	width;
	char	***s;
	char	**p;

	width = 0;
	s = data;
	while (*s)
	{
		p = *s;
		if (ft_strlen(p[n]) > width)
			width = ft_strlen(p[n]);
		s++;
	}
	return (width);
}

int		count_blocks(char ***data)
{
	char	***p;
	int		count_blocks;

	p = data;
	count_blocks = 0;
	while (*p)
	{
		count_blocks += ft_atoi((*p)[7]);
		p++;
	}
	return (count_blocks);
}
