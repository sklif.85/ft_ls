/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_files_in_multiple_columns.c                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 16:08:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 16:08:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	handle_print_files_in_multiple_columns(t_ls_objects **files,
		size_t max_objects_len, size_t count_column, size_t count_row)
{
	t_ls_objects	*file;
	int				i;
	int				pos;
	int				row;

	file = *files;
	i = -1;
	pos = 0;
	row = 0;
	while (file)
	{
		if (++i == pos)
		{
			ft_printf("%-*s\n", max_objects_len, file->path);
			pos += count_column;
			i++;
		}
		file = file->next;
		!file ? pos = ++row : 0;
		!file ? i = -1 : 0;
		!file ? file = *files : 0;
		if (pos == (int)count_row)
			break ;
	}
}

void	print_files_in_multiple_columns(t_ls_objects **files, size_t tty_width)
{
	size_t	max_objects_len;
	size_t	count_files;
	size_t	count_column;
	size_t	count_row;

	max_objects_len = get_max_file_len(files);
	count_files = get_count_file(files);
	count_column = get_count_column(max_objects_len, tty_width);
	count_row = get_count_row(count_files, max_objects_len, tty_width);
	handle_print_files_in_multiple_columns(files, max_objects_len,
							count_column, count_row);
}

size_t	get_max_file_len(t_ls_objects **files)
{
	t_ls_objects	*file;
	size_t			max_objects_len;
	size_t			len;

	max_objects_len = 0;
	file = *files;
	while (file)
	{
		len = ft_strlen(file->path);
		if (len > max_objects_len)
			max_objects_len = len;
		file = file->next;
	}
	return (max_objects_len);
}

size_t	get_count_file(t_ls_objects **files)
{
	t_ls_objects	*file;
	size_t			count_files;

	count_files = 0;
	file = *files;
	while (file)
	{
		count_files++;
		file = file->next;
	}
	return (count_files);
}
