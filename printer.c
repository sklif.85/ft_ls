/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 14:01:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/15 14:01:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

size_t	*get_total_width(t_ls_objects **objects)
{
	size_t *total_width;

	total_width = (size_t *)malloc(sizeof(size_t) * 4);
	total_width[0] = get_width((*objects)->object_data, 1);
	total_width[1] = get_width((*objects)->object_data, 2);
	total_width[2] = get_width((*objects)->object_data, 3);
	total_width[3] = get_width((*objects)->object_data, 4);
	return (total_width);
}

void	print_with_params(t_ls_objects **objects, int flags)
{
	char	***data;
	size_t	*total_width;

	data = (*objects)->object_data;
	if (!(*objects)->object_data)
		return ;
	if (!(flags & MASK_D))
		ft_printf("total %d\n", count_blocks((*objects)->object_data));
	if (flags & MASK_RL)
	{
		reverse_print(data, objects, flags);
		return ;
	}
	total_width = get_total_width(objects);
	while (*data)
	{
		microservice_printf(data, flags, total_width);
		data++;
	}
	free(total_width);
}

void	reverse_print(char ***data, t_ls_objects **objects, int flags)
{
	char	***p;
	size_t	*total_width;

	p = data;
	while (*p)
		p++;
	p--;
	total_width = get_total_width(objects);
	while (p != data)
	{
		microservice_printf(p, flags, total_width);
		p--;
	}
	microservice_printf(data, flags, get_total_width(objects));
	free(total_width);
}

void	microservice_printf(char ***data, int flags, size_t *total_width)
{
	if (!(flags & MASK_L))
	{
		if (flags & MASK_1)
			ft_printf("%s\n", (*data)[0]);
	}
	else if (flags & MASK_G)
		ft_printf("%s %*s  %*s  %*s %s %s\n", (*data)[0], total_width[0],
(*data)[1], total_width[2], (*data)[3], total_width[3],
(*data)[4], (*data)[5], (*data)[6]);
	else
		ft_printf("%s %*s %-*s  %-*s  %*s %s %s\n", (*data)[0], total_width[0],
(*data)[1], total_width[1], (*data)[2], total_width[2], (*data)[3],
total_width[3], (*data)[4], (*data)[5], (*data)[6]);
}

void	print_with_out_params(t_ls_objects **objects, int flags)
{
	char	***data;
	size_t	*total_width;

	data = (*objects)->object_data;
	if (!(*objects)->object_data)
	{
		ft_printf("\n");
		return ;
	}
	if (!(flags & MASK_1))
		total_width = get_total_width(objects);
	while (*data)
	{
		microservice_printf(data, flags, total_width);
		data++;
	}
	if (!(flags & MASK_1))
	{
		ft_printf("\n");
		free(total_width);
	}
}
