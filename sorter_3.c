/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorter_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orizhiy <orizhiy@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 20:27:00 by orizhiy           #+#    #+#             */
/*   Updated: 2017/10/22 20:27:00 by orizhiy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	make_sort(char ****object_data, int i)
{
	char	**p;

	p = (*object_data)[i - 1];
	(*object_data)[i - 1] = (*object_data)[i];
	(*object_data)[i] = p;
}
