#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: orizhiy <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/25 21:31:22 by orizhiy           #+#    #+#              #
#    Updated: 2017/08/29 21:03:02 by orizhiy          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

BLUE = echo "\033[0;36m"
RED = echo "\033[0;31m"
GREEN = echo "\033[0;32m"
END = echo "\033[0m"
PURPLE = echo "\033[0;35m"
BIWhite = echo "\033[1;97m"

NAME = ft_ls
G = gcc
FLAGS = -Wall -Wextra -Werror

SRC_PATH = ./
SRC_NAME = 	main.c stdout.c init.c sorter.c sorter_2.c sorter_3.c sub.c printer.c \
printer_2.c printer_3.c checker.c handle_objects.c processing_flags.c \
processing_flags_sub_1.c processing_flags_sub_2.c processing_flags_sub_3.c \
print_in_multiple_columns.c realloc.c print_files_in_multiple_columns.c \
errors_hendle.c \

OBJ_PATH = ./
OBJ_NAME = $(SRC_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

GCFLAGS = -I./asm_source -I ./libft/

LIBFT = ./libft/libft.a

.PHONY: all clean fclean re rmsh

all: $(NAME)

$(NAME): $(OBJ)
	@echo "$$($(PURPLE)) * FT_LS -> $$($(END))$$($(BLUE))Sources compiling...$$($(END))"
	@make -C libft/
	@$(G) $(FLAGS) $(GCFLAGS) -o $@ $(OBJ) $(LIBFT)
	@echo "$$($(PURPLE)) * FT_LS -> $$($(END))$$($(GREEN))Compile with SUCCESS!$$($(END))"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@$(G) $(FLAGS) $(GCFLAGS) -o $@ -c $<

clean:
	@make -C libft/ fclean
	@rm -f $(OBJ)
	@echo "$$($(PURPLE)) * FT_LS -> $$($(END))$$($(RED))Objects removed...$$($(END))"

fclean: clean
	@rm -f $(NAME)
	@echo "$$($(PURPLE)) * FT_LS -> $$($(END))$$($(RED))Binary removed...$$($(END))"

re: fclean all
	@make -C libft/ re

rmsh:
		rm *~
		rm \#*
		rm *.out
